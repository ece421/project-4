module Utils

  def getImageName(t)
    if t == "b"
      return "/blue.png"
    elsif t == "e"
      return "/empty.png"
    elsif t == "g"
      return "/green.png"
    elsif t == "O"
      return "/o.png"
    elsif t == "r"
      return "/red.png"
    elsif t == "T"
      return "/t.png"
    end
    return "/empty.png"
  end

end