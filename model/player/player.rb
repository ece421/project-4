require_relative '../../contracts/player_contracts'

class Player
	include PlayerContracts

	attr_reader :type, :name, :player_type

  HUMAN_PLAYER = 'HUMAN'
  AI_PLAYER = 'AI'

	# initialize player with a game piece type
	def initialize(type, name, board_size)
		initialize_preconditions(type, name, board_size)
    @type = type
    @name = name
    @size = board_size
	end

  # Gets column to put piece in
  def get_column(board)
  end

end