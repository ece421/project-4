require_relative "../model/game"
require_relative '../contracts/controller_contracts'
require_relative '../utils'

class GameController

  include ControllerContracts
	include Utils

	@view = nil
	@game = nil
	@builder = nil

  # Sets the view
	def set_view(view)
		@view = view
	end

  # Sets the builder
	def set_builder(builder)
		@builder = builder
	end

  # Sets up signal handlers and window
	def set_signal_handlers()
    set_signal_handlers_preconditions
		setStartingHandlers()
		setGameHandlers()
		window = @builder.get_object("window_launcher")
      	window.signal_connect( "destroy" ) { Gtk.main_quit }
		window_game = @builder.get_object("window_game")
		window_game.signal_connect( "destroy" ) { Gtk.main_quit }
		setDialog()
	end

	# Set error_dialog handler
	def setDialog()
		@dialog = @builder.get_object("dialogbox")
		@dialog.signal_connect( "destroy" ) { Gtk.main_quit }
		button_restart = @builder.get_object("button_restart")
		button_restart.signal_connect("clicked"){restartGame}
		button_exit = @builder.get_object("button_exitDialog")
		button_exit.signal_connect("clicked"){Gtk.main_quit}
	end
	# Handler for restartingGame
	def restartGame()
		clearBoard()
		@dialog.hide()
		window = @builder.get_object("window_launcher")
		window.show()
	end
	
	def clearBoard()
		 path =  File.expand_path("../resources", File.dirname(__FILE__))
		 		(0..41).each {|num|
    			tokenImage = @builder.get_object(num.to_s)
        	tokenImageName = "/empty.png"
        	tokenImage.set_file(path.to_s+tokenImageName)
        }
	end
	
  # Sets up signal handlers
	def setStartingHandlers()
    set_starting_handlers_preconditions
		buttonOK = @builder.get_object("button_ok")
		buttonCancel = @builder.get_object("button_cancel")
		buttonOK.signal_connect("clicked"){startingMenuOK}
		buttonCancel.signal_connect("clicked"){startingMenuCancel}
	end

  # sets up in game handlers
	def setGameHandlers()
    set_game_handlers_preconditions
		(1..7).each{ |col|
			buttonDropPiece = @builder.get_object("button_dropCol"+col.to_s)
			buttonDropPiece.signal_connect("clicked"){dropOnCol(col)}
		}
	end

  # Checks if someone has won
	def checkWin()
		@game.checkState
	end

  # Tries to drop a piece in the column
	def dropOnCol(col)

    label = @builder.get_object('label_currentStatus')
		if @game.colFull?(col-1)
      label.set_text('Column is full, try another')
    else
      label.set_text("#{@game.currentPlayer.name}'s turn")
			imageNum = @game.getIndexOf(col-1)
			@state = @game.checkState()
			if @state == Game::GAME_NOT_OVER
        drop_on_col_preconditions(col-1)
				@game.play_game(col-1)
				path =  File.expand_path("../resources", File.dirname(__FILE__))
				tokenImage = @builder.get_object(imageNum.to_s)

				tokenImage.set_file(path.to_s+getImageName(@game.currentPlayer.type))

        @state = @game.checkState
			  @game.switchPlayer() unless @state != Game::GAME_NOT_OVER
        updateStatusLabel(@game.currentPlayer)
			elsif @state == Game::GAME_OVER_WIN
				label.set_text(@game.currentPlayer.name+ " has won")
				setDialogMsg(@game.currentPlayer.name+ " has won")
				showDiag()
			elsif @state == Game::GAME_OVER_TIE
				label.set_text("It's a draw, everyone loses (or wins)")
				setDialogMsg("It's a draw, everyone loses (or wins)")
				showDiag()
			end
		end
		@state = @game.checkState()
		if @state == Game::GAME_OVER_WIN
			label.set_text(@game.currentPlayer.name+ " has won")
			setDialogMsg(@game.currentPlayer.name+ " has won")
			showDiag()
		elsif @state == Game::GAME_OVER_TIE
			label.set_text("ITS A TIE!")
			setDialogMsg("ITS A TIE!")
			showDiag()
		end
	end
	
	#makes diag appear
	def showDiag()
		@dialog.show()
	end

	# sets the message label in dialog box
	def setDialogMsg(t)
		label  = @builder.get_object('label_dialog')
		label.set_text(t)
	end

  # Ends game if cancel was chosen
	def startingMenuCancel
		Gtk.main_quit
	end

  # Updates status label for current player
	def updateStatusLabel(currentPlayer)
    update_status_label_preconditions
		label = @builder.get_object("label_currentStatus")
		status = " doing something!"
		label.set_text(currentPlayer.name.to_s+"'s turn")
	end 

  # Sets up new game on OK click
	def startingMenuOK
    starting_menu_ok_preconditions
		radiobutton_c4 = @builder.get_object("radiobutton_c4")
		radiobutton_otto = @builder.get_object("radiobutton_otto")
		radiobutton_human = @builder.get_object("radiobutton_human")
		radiobutton_ai = @builder.get_object("radiobutton_ai")
		radiobutton_dummy = @builder.get_object("radiobutton_dummy")
		radiobutton_smarty = @builder.get_object("radiobutton_smarty")

    text_box_player_1 = @builder.get_object('player_1_name')
    text_box_player_2 = @builder.get_object('player_2_name')


		#start logic
		gameType = -1
		if radiobutton_c4.active?
			gameType = 0
			player_1_token = 'b'
			player_2_token = 'r'
		else
			gameType = 1

			player_1_token = 'O'
			player_2_token = 'T'
    end

		player_1_name = text_box_player_1.text
		player_2_name = text_box_player_2.text

    if player_1_name.empty?
      player_1_name = 'Player 1'
    end

    if player_2_name.empty?
      player_2_name = 'Player 2'
    end

    status = @builder.get_object('label_currentStatus')
    status.text = "#{player_1_name}'s turn"

		@multiplayer = Game::SINGLEPLAYER
		if radiobutton_ai.active?
			@multiplayer = Game::MULTIPLAYER_SMART
			if radiobutton_dummy.active?
        @multiplayer = Game::MULTIPLAYER_DUMB
			end
		end
		window_game = @builder.get_object("window_game")
		window_game.show()
		window = @builder.get_object("window_launcher")
		window.hide()
		@game = Game.new(6,7,gameType,player_1_name,player_1_token, player_2_name,player_2_token, @multiplayer)
		@game.setView(@view)

    starting_menu_ok_postconditions
	end


		
end
