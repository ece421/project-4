gem 'minitest'

require 'minitest/autorun'
require File.dirname(__FILE__) + '/../model/board'

class BoardTests < MiniTest::Test

  def test_init_success
    b = Board.new(1,1)
  end

  def test_board_is_nice_size
    b = Board.new(4,80)
    assert_equal b.col_size, 80, "Column size wasn't good"
    assert_equal b.row_size, 4, "Row size isn't good"
    assert_equal b.grid.size, b.col_size, "Actual column size wasn't good"
    assert_equal b.grid[0].size, b.row_size, "Actual row size isn't that good"
  end

  def test_new_columns_arent_full
    b = Board.new(3,4)
    for i in 0..2
      assert !b.column_full?(1), "New column was full"
    end
  end

  def test_add_piece
    b = Board.new(4,4)
    b.add_piece(0, 1)
    assert_equal b.grid[0][0].type, 1, "Added piece wasn't of correct type"
    b.add_piece(0, 1)
    b.add_piece(0, 1)
    b.add_piece(0, 1)
    # Column 0 should be full now
    assert b.column_full?(0), "Column was not full"
  end

  def test_board_is_full
    b = Board.new(2,2)
    b.add_piece(0,1)
    b.add_piece(0,1)
    b.add_piece(1,1)
    b.add_piece(1,1)

    assert b.full?, "Board wasn't full"
  end

end