

gem 'test-unit'
require 'test/unit'
require_relative '../model/game'

module ViewLauncherContracts
  include Test::Unit::Assertions

  def class_invariant
    assert @builder != nil, "No nil builders"
    assert @controller != nil, "No nil controllers"
  end

  def initialize_preconditions(controller)
    assert controller != nil, "No nil controllers"
  end

  def update_preconditions(board_state)
    assert board_state != nil, "Invalid board state"
  end

  def update_board_images_preconditions(row, col, token, board_size)
    assert row >= 0, "Row too small"
    assert col >= 0, "Col too small"
    assert board_size > 0, "Board size too small"
    assert col <= board_size, "Col too big"
    assert token != nil, "No nil tokens"
  end

  def get_image_name_preconditions(t)
    assert t != nil, "Token be nil"
  end

end
