
gem 'test-unit'
require 'test/unit'

module PieceContracts
  include Test::Unit::Assertions

  def initialize_preconditions(type)
    assert type != nil, 'No nil type'
  end

  def class_invariant
    assert @type != nil, "No nil type"
  end

end