gem 'test-unit'
require 'test/unit'

module PlayerContracts
	include Test::Unit::Assertions

	def initialize_preconditions(type,name,board_size)
		assert type != nil , "You must have a game piece type!"
		assert name !=nil , "Im sure you have a name"
    assert board_size >0, "We want a bigger board please"
	end

  def board_preconditions(board)
    assert board != nil
  end

  def class_invariant
    assert @player_type == Player::HUMAN_PLAYER || @player_type == Player::AI_PLAYER, "Invalid playre type"
    assert @type != nil, "no nil type"
    assert @name != nil, "no nil name"
    assert @size > 0, "Bigger board please"
  end


end